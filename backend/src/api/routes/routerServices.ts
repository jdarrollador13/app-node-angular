import { Request, Response, NextFunction, Router } from 'express'
import ServicesController from '../../controllers/servicesController'
import { Container } from "typescript-ioc";


export default class routerServices {
  public app: Router
  constructor(router: Router) {
    this.app = router
  }
  router(): void {
  /**
   *@method : get
  * DEVUELVE SERVICO DE CIUDADES
  **/
    this.app.get(
      '/services/listado/ciudades/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.obtenerCiudades();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )

    this.app.post(
      '/services/insertar/datos/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.registrarDatos(req.body);
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )
  }
}