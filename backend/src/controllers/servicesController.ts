import { Request, Response, NextFunction } from 'express'
import { Inject } from "typescript-ioc";
import { ServicesDAO } from '../DAO/ServicesDAO'
import requests from 'request-promise'


export default class ServicesController {
	constructor(
		@Inject private servicesDAO: ServicesDAO,
	) {
	}
 /**
  * DEVUELVE SERVICO DE CIUDADES
	**/
	async obtenerCiudades():Promise<any> {
		let resp:any
    let url = `https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json`
    return resp = await requests.get(url,{
      json : true,
      method : 'GET'
    }).catch((error:any) => { 
      return  error
    })
	}

	async registrarDatos(requets:object|any): Promise<any> {
		let res:any;
		try {
			let {departamento,ciudad,nombre,correo} = requets
			if (departamento == '' && ciudad == '' && nombre == '' && correo == '') {
				res = { 'code' :400, 'msg' : 'campos obligatorios'}
				return res
			}
			let responseDao:any = await this.servicesDAO.registrarDatos(requets)
			return responseDao
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

}
