import Conection from '../loaders/databaseLoader'
import { Inject } from "typescript-ioc";

export class ServicesDAO {

	constructor(
		@Inject private databaseConnection: Conection
	) {
	}

	/**
	@router 
    **/
	public async registrarDatos(reqruest:object|any): Promise<any> {
		let data: any
		try {
			let {nombre,correo,departamento,ciudad} = reqruest
		  const connection = await this.databaseConnection.getPool()
		  //const query:any = await connection.query(`INSERT INTO ciudades ("ciudad","codigo") VALUES ($1, $2)`,[ciudad1,codigo])
			const query:any = await connection.query(`INSERT INTO contacts (name,email,state,city) VALUES($1, $2, $3, $4)`, [nombre,correo,departamento,ciudad]);
			console.log(query,'query------')
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}
}