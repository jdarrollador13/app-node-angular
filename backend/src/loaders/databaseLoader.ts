//import mysql from 'mysql';
import { Pool } from 'pg'
import { Singleton } from 'typescript-ioc';
import config from '../config';

/**
 * @category Database
 */
@Singleton
export default class DatabaseConnection {
    constructor() {
       
    }

    public async getPool() {
      // const connection = new Pool({
      //   host: '178.128.146.252',
      //   user: 'admin_sigmatest',
      //   database: 'admin_sigmauser',
      //   password: 'pfaDKIJyPF',
      //   max: 20,
      //   idleTimeoutMillis: 30000,
      //   connectionTimeoutMillis: 2000,
      // })
      // return connection 

      const connection = new Pool({
        host: 'localhost',
        user: 'postgres',
        database: 'parqueadero',
        password: 'admin',
        max: 20,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 2000,
      })
      return connection 
    }
}