import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ServiciosEndpointService } from './servicios/servicios-endpoint.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend'
  formOrden:FormGroup
  submitted:boolean = false
  respuesta:object|any
  inputResultado:boolean = false
  inputValue:string|any
  valor:string|any = ''
  departamento:object|any = []
  res:[]|any
  ciudad:[]|any =[]
  errorFormulario:boolean = false
  private ejecutar:boolean = false
  private URL = 'http://127.0.0.1:3000/api/v1/services/listado/ciudades'
  private parametros:Array<any> = []


  constructor(private formBuilder: FormBuilder,
  	private serviciosEndpointService: ServiciosEndpointService){
  	//[Validators.required, Validators.minLength(10)]
  	this.formOrden = this.formBuilder.group({
      departamento: ['', Validators.required],
      ciudad : ['', Validators.required],
      nombre : ['', [Validators.required, Validators.maxLength(50)]],
      correo : ['', [Validators.required, Validators.maxLength(50)]]
    });
  }

  async ngOnInit() {
    await this.obtenerDepartamentos()
  }

  obtenerDepartamentos() {
  	this.serviciosEndpointService.getData('http://127.0.0.1:3000/api/v1/services/listado/ciudades/')
  	.toPromise().then(data => {
        this.res = data
        this.departamento = Object.keys(this.res)
      })
  }

  cargueCiudad(e:any){
  	let arrCiudad:[]|any = []
  	for(const [key, value] of Object.entries(this.res)){
  		if(key == e.target.value) {
  			this.ciudad = value
  		}
  	}
  }

  get f() {
    return this.formOrden.controls;
  }

  guardarDatos(event) {
  	console.log(this.formOrden.value)
    if (this.formOrden.invalid) {
      this.submitted = true;
      return;
    } else {
    	this.serviciosEndpointService.saveData('http://127.0.0.1:3000/api/v1/services/insertar/datos/',this.formOrden.value)
    	.toPromise().then(data => {
    		let res:object|any = data
    		if(res.code == 200){
    			this.formOrden.reset()
    		}else {
    			this.errorFormulario = true
    		}
      })
    }
  }

}
